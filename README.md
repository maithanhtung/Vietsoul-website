# Vietsoul-website
# http://sw.meomeow.com/
## Scope of the Project 
Viet Soul is an e-commerce site. It provides a range of Vietnamese traditional items that were made by hand.
The scope of project is the completion of a client side that receives user input and performs user interfaces, a server-side will update and process requests. The web shop is built for not only customer but also shop owner (Admin) use cases

## User Inteface
* Responsive
* Minimalist Style

## Database
* Version control: Migrations make:migration
* Stored in MySQL
* Tables: Users, Product, Orders, FAQs, Messages

## Client Side SW Design

#####Client Browser
* FireFox v3.5+
* Internet Explorer 9+
* Chrome
* Safari 4+

#####Language
* HTML5
* CSS
* JavaScript

#####Framework and library
* Bootstrap
* Materializecss
* jQuery
   	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.parallax-1.1.3.js"></script>
	<script src="assets/js/imagesloaded.pkgd.js"></script>
	<script src="assets/js/jquery.sticky.js"></script>
	<script src="assets/js/smoothscroll.js"></script>
	<script src="assets/js/wow.min.js"></script>
  <script src="assets/js/jquery.easypiechart.js"></scripti
  <script src="assets/js/waypoints.min.js"></script>
  <script src="assets/js/jquery.cbpQTRotator.js"></script>
* Css libraries
  	<link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="assets/css/animate.css" rel="stylesheet">

## Server Side SW Design
* MVC design pattern 
* Laravel 5 framework 
